#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdlib.h>
#include "99-myutils.h"

#define offset 1000
#define T_REHAT 2000

typedef  struct {
   sem_t mutex, mutex2, start, ready;
   int gIDLoop, n, t, all;
} globalStruct;

globalStruct* globalVar;

int idmaster = 0;
int rPID, rPPID, id1000, temp;

void cetak(int id1000,int tID){
   printf("PID[%d] \t ThreadID \t [%4.4d]\n",
      getpid()-id1000+offset, tID);
}

void* tritManager (void* a){
   // printf("mgrID: %d\n",getpid()-id1000+offset);
   // for(int j=0;j<all;j++){
   //    printf("nt: %d\n",all);
   //    sem_wait(&globalVar->ready);
   // }
   // printf("all trit globalVar->ready\n");
   // for(int j=0;j<all;j++){
   //    sem_post(&globalVar->start);
   // }
   // printf("start allowed\n");
}

void* trit (void* a) {
   sem_wait(&globalVar->mutex);
   int tID = getADDglobalID(globalVar->gIDLoop);
   sem_post(&globalVar->mutex);
   // printf("[%d] %d ready\n", getpid()-id1000+offset,tID);
   // sem_post(&globalVar->ready);
   // sem_wait(&globalVar->start);
   while(TRUE){
      rehat_acak(T_REHAT);
      cetak(id1000,tID);
   }
}

void main(int argc, char * argv[])
{  
   int forkVal;
   id1000=getpid();
   globalVar = mmap(NULL, sizeof(globalStruct), PROT_READ | PROT_WRITE, 
                    MAP_SHARED | MAP_ANONYMOUS, -1, 0);
   init_globalID();
   sem_init(&globalVar->mutex,1,1);
   sem_init(&globalVar->mutex2,1,1);
   sem_init(&globalVar->start,1,0);
   sem_init(&globalVar->ready,1,0);
   printf("Hello... \n");
   printf("Please input number of processes (N) : ");
   scanf("%d", &globalVar->n);
   printf("Please input number of threads per process (T) : ");
   scanf("%d", &globalVar->t);
   printf("The input N is : %d \n", globalVar->n);
   printf("The input T is : %d \n", globalVar->t);
   globalVar->all = globalVar->n*globalVar->t;

   for(int i = 0;i<globalVar->t;i++){
      // printf("bikin trit\n");
      daftar_trit(trit);
   }
   // printf("gidloop: %d at [%d]\n",*gIDLoop,getpid()-id1000+offset);
   do {
      if(!(forkVal=fork())){
         sem_wait(&globalVar->mutex2);
         rehat_acak(T_REHAT);
         globalVar->gIDLoop+=1;
         // printf("gidloop: %d at [%d]\n",*gIDLoop,getpid()-id1000+offset);
         for(int k = 0;k<globalVar->t;k++){
            // printf("bikin trit\n");
            daftar_trit(trit);
         }
         sem_post(&globalVar->mutex2);
         break; //break or return;
      }
   } while(--globalVar->n>1);
   sleep(1);
   // printf("%d keluar loop\n",getpid()-id1000+offset);
   // if(forkVal) {
      // printf("daftarmgr: %d\n",getpid()-id1000+offset);
      daftar_trit(tritManager);
   // }
   jalankan_trit();
   wait(NULL);
   beberes_trit("Selesai...");
}
