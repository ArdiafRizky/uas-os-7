#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "99-myutils.h"

#define T_REHAT 2000

#define MYFLAGS     O_CREAT | O_RDWR
#define MYPROTECT PROT_READ | PROT_WRITE
#define MYVISIBILITY          MAP_SHARED
#define SFILE             "demo-file.bin"

#define NN 4

typedef  struct {
    sem_t  buffmutex;
    int    buffsize;
    int    buffer;
} myshare;

myshare* mymap;
int idmaster_prod=0;
int idmaster_cons=0;

void* producer (void* a) {
    int id_prod = ++idmaster_prod;

    while(TRUE){
      rehat_acak(T_REHAT);
      sem_wait(&mymap->buffmutex);
      mymap->buffer = rand() % 1000;
      printf("PID[%4.4d]\t Thread[%4.4d]\t PRODUCE %3.3d\t BUFFERSIZE  [%4.4d]\n", getpid(), id_prod, mymap->buffer, ++mymap->buffsize);
      fflush(NULL);
      sem_post(&mymap->buffmutex);
    }

}


void* consumer (void* a) {
    int id_cons = ++idmaster_cons;

    while(TRUE){
        rehat_acak(T_REHAT);
        sem_wait(&mymap->buffmutex);
        mymap->buffer = rand() % 1000;
        printf("PID[%4.4d]\t Thread[%4.4d]\t CONSUME %3.3d\t BUFFERSIZE  [%4.4d]\n", getpid(), id_cons, mymap->buffer, --mymap->buffsize);
        fflush(NULL);
        sem_post(&mymap->buffmutex);
   }
}


int main(int argc, char * argv[])
{
   int fd = open(SFILE, MYFLAGS);
   mymap =  mmap(NULL, sizeof(myshare), MYPROTECT, MYVISIBILITY, fd, 0);
   sem_init(&mymap->buffmutex, 1, 1);

   if (fork()){
      for (int i=0; i < NN; i++){
        daftar_trit(producer);
      }
   } else {
      for (int i=0; i < NN; i++){
        daftar_trit(consumer);
      }
   }
    
   jalankan_trit();
   beberes_trit("Selesai...");
}


