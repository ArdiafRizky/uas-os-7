#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

void store(int id1000,int offset){
   printf("PID[%d] \t PPID[%4d]\n",
      getpid()-id1000+offset,
      getppid()-id1000+offset);
}

void main(void) {

    int offset = 1000;
    int rPID, rPPID, id1000=getpid();
    // 0 to 1
    if(!fork()){
        store(id1000,offset);
        // 1 to 2
        if(!fork()){
            store(id1000,offset);
            return;
        }
        // 1 to 3
        if(!fork()){
            store(id1000,offset);
            return;
        }
        wait(NULL);
        return;
    }
    wait(NULL);
    rPID=getpid()-id1000+offset;
    rPPID=getppid()-id1000+offset;
    if (rPPID < 1000 || rPPID > rPID) rPPID=999;
    printf("PID[%d] \t PPID[%4d]\n", rPID, rPPID);
    fflush(NULL);
    // 0 to 4
    if(!fork()){
        store(id1000,offset);
        // 4 to 5
        if(!fork()){
            store(id1000,offset);
            return;
        }
        // 4 to 6
        if(!fork()){
            store(id1000,offset);
            return;
        }
        wait(NULL);
        return;
    }
    wait(NULL);
    return;
}

