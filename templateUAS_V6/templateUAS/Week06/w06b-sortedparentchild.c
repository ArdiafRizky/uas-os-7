#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/mman.h>

typedef struct {
   char *strarr[7][32];
   int count;
} strArray;

strArray* strmap;

void store(int i, int id1000,int offset){
   sprintf(strmap->strarr[i],"PID[%d] \t PPID[%4d]\n",
      getpid()-id1000+offset,
      getppid()-id1000+offset);
}

void main(void) {
   int offset = 1000;
   int rPID, rPPID, count=0;
   int id1000=getpid();
   strmap = mmap(NULL, sizeof(strArray), PROT_READ | PROT_WRITE, 
                    MAP_SHARED | MAP_ANONYMOUS, -1, 0);
   
   //1000 fork to 1001
   if(!fork()){
      store(++strmap->count,id1000,offset);
      return;
   }
   wait(NULL);
   //1000 fork to 1002
   if(!fork()){
      store(++strmap->count,id1000,offset);
      //1002 fork to 1003
      if(!fork()){
         store(++strmap->count,id1000,offset);
         return;
      }
      wait(NULL);
      //1002 fork to 1004
      if(!fork()){
         store(++strmap->count,id1000,offset);
         //1004 fork to 1005
         if(!fork()){
            store(++strmap->count,id1000,offset);
            return;
         }
         wait(NULL);
         //1004 fork to 1006
         if(!fork()){
            store(++strmap->count,id1000,offset);
            return;
         }
         wait(NULL);
         return;
      }
      wait(NULL);
      return;
   }
   wait(NULL);
   rPID=getpid()-id1000+offset;
   rPPID=getppid()-id1000+offset;
   if (rPPID < 1000 || rPPID > rPID) rPPID=999;
   for(int ii=6;ii>0;ii--){
      printf("%s",strmap->strarr[ii]);
   }
   printf("PID[%d] \t PPID[%4d]\n", rPID, rPPID);
   return;
}