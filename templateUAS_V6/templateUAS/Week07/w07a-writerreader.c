#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include "99-myutils.h"

sem_t syncW1, syncR1, syncR2, syncW2;

void* T1 (void* a) {
   while(TRUE){
      sleep(1);
      sem_wait(&syncW1);
      printf("T1 \t Writer\n");
      sem_post(&syncW2);
   }
}


void* T2 (void* a) {
   while(TRUE){
      sem_wait(&syncW2);
      printf("T2 \t Writer\n");
      sem_post(&syncR1);
   }
}


void* T3 (void* a) {
   while(TRUE){
      sem_wait(&syncR1);
      printf("T3 \t Reader\n");
      sem_post(&syncR2);
   }
}

void* T4 (void* a) {
   while(TRUE){
      sleep(1);
      sem_wait(&syncR2);
      printf("T4 \t Reader\n");
      sem_post(&syncW1);
   }
}

int main(int argc, char * argv[])
{
   sem_init(&syncW1,0,1);
   sem_init(&syncR1,0,0);
   sem_init(&syncW2,0,0);
   sem_init(&syncR2,0,0);
   daftar_trit(T1);
   daftar_trit(T2);
   daftar_trit(T3);
   daftar_trit(T4);
   jalankan_trit();
   beberes_trit("Selesai...");
}
