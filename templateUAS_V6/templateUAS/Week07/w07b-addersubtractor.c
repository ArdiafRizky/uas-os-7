#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include "99-myutils.h"


#define T_REHAT 2000

int idmaster = 0;
int sharedInt = 0;

void  flushsleep(int ii) {
   fflush(NULL);
   sleep (ii  );
}

void* adder (void* a) {
   //Ambil ID dari Global Variable
   int id = idmaster;
   //Naikan ID nya
   idmaster = idmaster + 1;
   while (TRUE){
      //Ambil Nilai Random
      int random_value = (rand() % 5) + 1;
      //Randomized Delay
      rehat_acak(random_value);
      //Tambahkan Nilai SharedInt
      sharedInt = sharedInt + random_value;
      //Print ID, Ammount of Add/Subtract, Current Value
      printf("Thread[%1.3d]   ADD   %1.3d   CURRENT VALUE   [%1.3d]\n", id, random_value,sharedInt);
      flushsleep(1);
      rehat_acak(random_value * 100 * 100 * 100);
   }
   return 0;
}


void* substractor (void* a) {
   //Ambil ID dari Global Variable
   int id = idmaster;
   //Naikan ID nya
   idmaster = idmaster + 1;
   while (TRUE){
      //Ambil Nilai Random
      int random_value = (rand() % 5) + 1;
      //Randomized Delay
      rehat_acak(random_value);
      //Kurangkan Nilai SharedInt
      sharedInt = sharedInt - random_value;
      //Print ID, Ammount of Add/Subtract, Current Value
      printf("Thread[%1.3d]   SUB   %1.3d   CURRENT VALUE   [%1.3d]\n", id, random_value,sharedInt);
      flushsleep(1);
      rehat_acak(random_value * 100 * 100 * 100);
   }
   return 0;
}

int main(int argc, char * argv[])
{  
   int n;


   printf("Hello... \n");
   printf("Please input a positive number (N) : ");
   scanf("%d", &n);
   printf("The input N is : %d \n", n);

   int i;
   //Loop sebanyak N agar hasilnya 2*N
   for (i=0;i<n;i++){
      //Daftarkan Thread Adder & Subtractor
      daftar_trit(adder);
      daftar_trit(substractor);
   }     
   jalankan_trit();
}
