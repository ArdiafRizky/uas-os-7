#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>


#define OFILE1 "output_problem_b1.txt"
#define OFILE2 "output_problem_b2.txt"

#define STRING_LIMIT 100


void main(void) {
   int n, loc, which_fd, fd1, fd2;

   fd1 = open (OFILE1, O_RDWR | O_CREAT, 0644);
   fd2 = open (OFILE2, O_RDWR | O_CREAT, 0644);

   char text[STRING_LIMIT];
   
   scanf("%d", &n);

   for (int i = 0; i < n; i++){
      scanf("%s %d %d", &text, &loc, &which_fd);
      if (which_fd == 1){
         lseek(fd1, loc, 0);
         write(fd1, text, strlen(text));

      } else {
         lseek(fd2, loc, 0);
         write(fd2, text, strlen(text));

      }

   }
   close(fd1);
   close(fd2);
}
