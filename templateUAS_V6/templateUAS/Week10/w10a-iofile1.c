#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>


#define OFILE "output_problem_a.txt"
#define STRING_LIMIT 100


void main(void) {
   int n, loc, fd;

   fd = open (OFILE, O_RDWR | O_CREAT, 0644);

   char text[STRING_LIMIT];
   
   scanf("%d", &n);

   for (int i = 0; i < n; i++){
      scanf("%s %d", &text, &loc);
      lseek(fd, loc, 0);
      write(fd, text, strlen(text));
   }
   close(fd);
}
